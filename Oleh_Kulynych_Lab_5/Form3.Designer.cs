﻿namespace Oleh_Kulynych_Lab_5
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_back = new System.Windows.Forms.Button();
            this.gd_AddAbonent = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_Contacts_Abonent = new System.Windows.Forms.TextBox();
            this.tb_PIB_Abonent = new System.Windows.Forms.TextBox();
            this.b_AddNewAbonent = new System.Windows.Forms.Button();
            this.gb_AddOperator = new System.Windows.Forms.GroupBox();
            this.dtp_date_employment_Operator = new System.Windows.Forms.DateTimePicker();
            this.b_AddOperator = new System.Windows.Forms.Button();
            this.tb_numcalls_Operator = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_PIB_Operator = new System.Windows.Forms.TextBox();
            this.gb_AddNewHotline = new System.Windows.Forms.GroupBox();
            this.b_AddNewHotline = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpEndtime = new System.Windows.Forms.DateTimePicker();
            this.dtp_Starttime = new System.Windows.Forms.DateTimePicker();
            this.tb_NameHotline = new System.Windows.Forms.TextBox();
            this.gb_NewAplication = new System.Windows.Forms.GroupBox();
            this.dtp_date_finish_aplication = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dtp_date_start_aplication = new System.Windows.Forms.DateTimePicker();
            this.b_AddNewAplication = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtp_time_finish_aplication = new System.Windows.Forms.DateTimePicker();
            this.dtp_time_start_aplication = new System.Windows.Forms.DateTimePicker();
            this.tb_topic_aplication = new System.Windows.Forms.TextBox();
            this.cb_Operator = new System.Windows.Forms.ComboBox();
            this.cb_Abonent = new System.Windows.Forms.ComboBox();
            this.cb_Hotline = new System.Windows.Forms.ComboBox();
            this.gd_AddAbonent.SuspendLayout();
            this.gb_AddOperator.SuspendLayout();
            this.gb_AddNewHotline.SuspendLayout();
            this.gb_NewAplication.SuspendLayout();
            this.SuspendLayout();
            // 
            // b_back
            // 
            this.b_back.Location = new System.Drawing.Point(29, 401);
            this.b_back.Name = "b_back";
            this.b_back.Size = new System.Drawing.Size(166, 23);
            this.b_back.TabIndex = 1;
            this.b_back.Text = "Повернутись назад";
            this.b_back.UseVisualStyleBackColor = true;
            this.b_back.Click += new System.EventHandler(this.b_back_Click);
            // 
            // gd_AddAbonent
            // 
            this.gd_AddAbonent.Controls.Add(this.label2);
            this.gd_AddAbonent.Controls.Add(this.label1);
            this.gd_AddAbonent.Controls.Add(this.tb_Contacts_Abonent);
            this.gd_AddAbonent.Controls.Add(this.tb_PIB_Abonent);
            this.gd_AddAbonent.Controls.Add(this.b_AddNewAbonent);
            this.gd_AddAbonent.Location = new System.Drawing.Point(29, 12);
            this.gd_AddAbonent.Name = "gd_AddAbonent";
            this.gd_AddAbonent.Size = new System.Drawing.Size(314, 124);
            this.gd_AddAbonent.TabIndex = 20;
            this.gd_AddAbonent.TabStop = false;
            this.gd_AddAbonent.Text = "Add new Abonent";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(168, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Контакти абонента";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "ПІБ Абонента";
            // 
            // tb_Contacts_Abonent
            // 
            this.tb_Contacts_Abonent.Location = new System.Drawing.Point(171, 46);
            this.tb_Contacts_Abonent.Name = "tb_Contacts_Abonent";
            this.tb_Contacts_Abonent.Size = new System.Drawing.Size(137, 22);
            this.tb_Contacts_Abonent.TabIndex = 2;
            this.tb_Contacts_Abonent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_Contacts_Abonent_KeyPress);
            // 
            // tb_PIB_Abonent
            // 
            this.tb_PIB_Abonent.Location = new System.Drawing.Point(7, 46);
            this.tb_PIB_Abonent.Name = "tb_PIB_Abonent";
            this.tb_PIB_Abonent.Size = new System.Drawing.Size(146, 22);
            this.tb_PIB_Abonent.TabIndex = 1;
            // 
            // b_AddNewAbonent
            // 
            this.b_AddNewAbonent.Location = new System.Drawing.Point(7, 86);
            this.b_AddNewAbonent.Name = "b_AddNewAbonent";
            this.b_AddNewAbonent.Size = new System.Drawing.Size(146, 23);
            this.b_AddNewAbonent.TabIndex = 0;
            this.b_AddNewAbonent.Text = "Додати абонента";
            this.b_AddNewAbonent.UseVisualStyleBackColor = true;
            this.b_AddNewAbonent.Click += new System.EventHandler(this.b_AddNewAbonent_Click);
            // 
            // gb_AddOperator
            // 
            this.gb_AddOperator.Controls.Add(this.dtp_date_employment_Operator);
            this.gb_AddOperator.Controls.Add(this.b_AddOperator);
            this.gb_AddOperator.Controls.Add(this.tb_numcalls_Operator);
            this.gb_AddOperator.Controls.Add(this.label5);
            this.gb_AddOperator.Controls.Add(this.label4);
            this.gb_AddOperator.Controls.Add(this.label3);
            this.gb_AddOperator.Controls.Add(this.tb_PIB_Operator);
            this.gb_AddOperator.Location = new System.Drawing.Point(350, 12);
            this.gb_AddOperator.Name = "gb_AddOperator";
            this.gb_AddOperator.Size = new System.Drawing.Size(438, 124);
            this.gb_AddOperator.TabIndex = 21;
            this.gb_AddOperator.TabStop = false;
            this.gb_AddOperator.Text = "Add new Operator";
            // 
            // dtp_date_employment_Operator
            // 
            this.dtp_date_employment_Operator.CustomFormat = "dd.MM.yyyy";
            this.dtp_date_employment_Operator.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_date_employment_Operator.Location = new System.Drawing.Point(214, 46);
            this.dtp_date_employment_Operator.Name = "dtp_date_employment_Operator";
            this.dtp_date_employment_Operator.Size = new System.Drawing.Size(200, 22);
            this.dtp_date_employment_Operator.TabIndex = 4;
            this.dtp_date_employment_Operator.Value = new System.DateTime(2020, 11, 19, 0, 0, 0, 0);
            // 
            // b_AddOperator
            // 
            this.b_AddOperator.Location = new System.Drawing.Point(214, 86);
            this.b_AddOperator.Name = "b_AddOperator";
            this.b_AddOperator.Size = new System.Drawing.Size(169, 23);
            this.b_AddOperator.TabIndex = 6;
            this.b_AddOperator.Text = "Додати оператора";
            this.b_AddOperator.UseVisualStyleBackColor = true;
            this.b_AddOperator.Click += new System.EventHandler(this.b_AddOperator_Click);
            // 
            // tb_numcalls_Operator
            // 
            this.tb_numcalls_Operator.Location = new System.Drawing.Point(23, 95);
            this.tb_numcalls_Operator.Name = "tb_numcalls_Operator";
            this.tb_numcalls_Operator.Size = new System.Drawing.Size(100, 22);
            this.tb_numcalls_Operator.TabIndex = 5;
            this.tb_numcalls_Operator.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_numcalls_Operator_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Число дзвінків";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(211, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(172, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Дата прийому на роботу";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "ПІБ Оператора";
            // 
            // tb_PIB_Operator
            // 
            this.tb_PIB_Operator.Location = new System.Drawing.Point(20, 45);
            this.tb_PIB_Operator.Name = "tb_PIB_Operator";
            this.tb_PIB_Operator.Size = new System.Drawing.Size(170, 22);
            this.tb_PIB_Operator.TabIndex = 0;
            // 
            // gb_AddNewHotline
            // 
            this.gb_AddNewHotline.Controls.Add(this.b_AddNewHotline);
            this.gb_AddNewHotline.Controls.Add(this.label8);
            this.gb_AddNewHotline.Controls.Add(this.label7);
            this.gb_AddNewHotline.Controls.Add(this.label6);
            this.gb_AddNewHotline.Controls.Add(this.dtpEndtime);
            this.gb_AddNewHotline.Controls.Add(this.dtp_Starttime);
            this.gb_AddNewHotline.Controls.Add(this.tb_NameHotline);
            this.gb_AddNewHotline.Location = new System.Drawing.Point(29, 143);
            this.gb_AddNewHotline.Name = "gb_AddNewHotline";
            this.gb_AddNewHotline.Size = new System.Drawing.Size(314, 236);
            this.gb_AddNewHotline.TabIndex = 22;
            this.gb_AddNewHotline.TabStop = false;
            this.gb_AddNewHotline.Text = "Add new Hotline";
            // 
            // b_AddNewHotline
            // 
            this.b_AddNewHotline.Location = new System.Drawing.Point(7, 198);
            this.b_AddNewHotline.Name = "b_AddNewHotline";
            this.b_AddNewHotline.Size = new System.Drawing.Size(126, 23);
            this.b_AddNewHotline.TabIndex = 6;
            this.b_AddNewHotline.Text = "Додати лінію";
            this.b_AddNewHotline.UseVisualStyleBackColor = true;
            this.b_AddNewHotline.Click += new System.EventHandler(this.b_AddNewHotline_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(211, 17);
            this.label8.TabIndex = 5;
            this.label8.Text = "Виберіть час кінця роботи лінії";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(227, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "Виберіть час початку роботи лінї";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Введіть назву лінії";
            // 
            // dtpEndtime
            // 
            this.dtpEndtime.CustomFormat = "HH:mm:00";
            this.dtpEndtime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpEndtime.Location = new System.Drawing.Point(7, 144);
            this.dtpEndtime.Name = "dtpEndtime";
            this.dtpEndtime.ShowUpDown = true;
            this.dtpEndtime.Size = new System.Drawing.Size(98, 22);
            this.dtpEndtime.TabIndex = 2;
            this.dtpEndtime.TabStop = false;
            // 
            // dtp_Starttime
            // 
            this.dtp_Starttime.Checked = false;
            this.dtp_Starttime.CustomFormat = "HH:mm:00";
            this.dtp_Starttime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtp_Starttime.Location = new System.Drawing.Point(7, 93);
            this.dtp_Starttime.Name = "dtp_Starttime";
            this.dtp_Starttime.ShowUpDown = true;
            this.dtp_Starttime.Size = new System.Drawing.Size(98, 22);
            this.dtp_Starttime.TabIndex = 1;
            this.dtp_Starttime.Value = new System.DateTime(2020, 11, 18, 18, 53, 24, 0);
            // 
            // tb_NameHotline
            // 
            this.tb_NameHotline.Location = new System.Drawing.Point(7, 44);
            this.tb_NameHotline.Name = "tb_NameHotline";
            this.tb_NameHotline.Size = new System.Drawing.Size(298, 22);
            this.tb_NameHotline.TabIndex = 0;
            // 
            // gb_NewAplication
            // 
            this.gb_NewAplication.Controls.Add(this.dtp_date_finish_aplication);
            this.gb_NewAplication.Controls.Add(this.label16);
            this.gb_NewAplication.Controls.Add(this.label15);
            this.gb_NewAplication.Controls.Add(this.dtp_date_start_aplication);
            this.gb_NewAplication.Controls.Add(this.b_AddNewAplication);
            this.gb_NewAplication.Controls.Add(this.label14);
            this.gb_NewAplication.Controls.Add(this.label13);
            this.gb_NewAplication.Controls.Add(this.label12);
            this.gb_NewAplication.Controls.Add(this.label11);
            this.gb_NewAplication.Controls.Add(this.label10);
            this.gb_NewAplication.Controls.Add(this.label9);
            this.gb_NewAplication.Controls.Add(this.dtp_time_finish_aplication);
            this.gb_NewAplication.Controls.Add(this.dtp_time_start_aplication);
            this.gb_NewAplication.Controls.Add(this.tb_topic_aplication);
            this.gb_NewAplication.Controls.Add(this.cb_Operator);
            this.gb_NewAplication.Controls.Add(this.cb_Abonent);
            this.gb_NewAplication.Controls.Add(this.cb_Hotline);
            this.gb_NewAplication.Location = new System.Drawing.Point(350, 143);
            this.gb_NewAplication.Name = "gb_NewAplication";
            this.gb_NewAplication.Size = new System.Drawing.Size(438, 295);
            this.gb_NewAplication.TabIndex = 23;
            this.gb_NewAplication.TabStop = false;
            this.gb_NewAplication.Text = "Add new Aplication";
            // 
            // dtp_date_finish_aplication
            // 
            this.dtp_date_finish_aplication.CustomFormat = "";
            this.dtp_date_finish_aplication.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_date_finish_aplication.Location = new System.Drawing.Point(26, 234);
            this.dtp_date_finish_aplication.Name = "dtp_date_finish_aplication";
            this.dtp_date_finish_aplication.Size = new System.Drawing.Size(178, 22);
            this.dtp_date_finish_aplication.TabIndex = 4;
            this.dtp_date_finish_aplication.Value = new System.DateTime(2020, 11, 19, 0, 0, 0, 0);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 214);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(201, 17);
            this.label16.TabIndex = 15;
            this.label16.Text = "Дата завершення звернення";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 169);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(173, 17);
            this.label15.TabIndex = 14;
            this.label15.Text = "Дата початку звернення";
            // 
            // dtp_date_start_aplication
            // 
            this.dtp_date_start_aplication.CustomFormat = "";
            this.dtp_date_start_aplication.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_date_start_aplication.Location = new System.Drawing.Point(26, 189);
            this.dtp_date_start_aplication.Name = "dtp_date_start_aplication";
            this.dtp_date_start_aplication.Size = new System.Drawing.Size(178, 22);
            this.dtp_date_start_aplication.TabIndex = 4;
            this.dtp_date_start_aplication.Value = new System.DateTime(2020, 11, 19, 0, 0, 0, 0);
            // 
            // b_AddNewAplication
            // 
            this.b_AddNewAplication.Location = new System.Drawing.Point(253, 262);
            this.b_AddNewAplication.Name = "b_AddNewAplication";
            this.b_AddNewAplication.Size = new System.Drawing.Size(179, 27);
            this.b_AddNewAplication.TabIndex = 12;
            this.b_AddNewAplication.Text = "Додати звернення";
            this.b_AddNewAplication.UseVisualStyleBackColor = true;
            this.b_AddNewAplication.Click += new System.EventHandler(this.b_AddNewAplication_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(250, 214);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(192, 17);
            this.label14.TabIndex = 11;
            this.label14.Text = "Час завершення звернення";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(250, 169);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(164, 17);
            this.label13.TabIndex = 10;
            this.label13.Text = "Час початку звернення";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(26, 121);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(166, 17);
            this.label12.TabIndex = 9;
            this.label12.Text = "Введіть тему звернення";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(141, 17);
            this.label11.TabIndex = 8;
            this.label11.Text = "Виберіть оператора";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(214, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(133, 17);
            this.label10.TabIndex = 7;
            this.label10.Text = "Виберіть абонента";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Виберіть лінію";
            // 
            // dtp_time_finish_aplication
            // 
            this.dtp_time_finish_aplication.CustomFormat = "";
            this.dtp_time_finish_aplication.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtp_time_finish_aplication.Location = new System.Drawing.Point(253, 234);
            this.dtp_time_finish_aplication.Name = "dtp_time_finish_aplication";
            this.dtp_time_finish_aplication.ShowUpDown = true;
            this.dtp_time_finish_aplication.Size = new System.Drawing.Size(85, 22);
            this.dtp_time_finish_aplication.TabIndex = 5;
            this.dtp_time_finish_aplication.Value = new System.DateTime(2020, 11, 19, 20, 39, 21, 0);
            // 
            // dtp_time_start_aplication
            // 
            this.dtp_time_start_aplication.CustomFormat = "";
            this.dtp_time_start_aplication.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtp_time_start_aplication.Location = new System.Drawing.Point(253, 189);
            this.dtp_time_start_aplication.Name = "dtp_time_start_aplication";
            this.dtp_time_start_aplication.ShowUpDown = true;
            this.dtp_time_start_aplication.Size = new System.Drawing.Size(85, 22);
            this.dtp_time_start_aplication.TabIndex = 4;
            this.dtp_time_start_aplication.Value = new System.DateTime(2020, 11, 19, 20, 38, 49, 0);
            // 
            // tb_topic_aplication
            // 
            this.tb_topic_aplication.Location = new System.Drawing.Point(23, 144);
            this.tb_topic_aplication.Name = "tb_topic_aplication";
            this.tb_topic_aplication.Size = new System.Drawing.Size(372, 22);
            this.tb_topic_aplication.TabIndex = 3;
            // 
            // cb_Operator
            // 
            this.cb_Operator.FormattingEnabled = true;
            this.cb_Operator.Location = new System.Drawing.Point(23, 91);
            this.cb_Operator.Name = "cb_Operator";
            this.cb_Operator.Size = new System.Drawing.Size(181, 24);
            this.cb_Operator.TabIndex = 2;
            // 
            // cb_Abonent
            // 
            this.cb_Abonent.FormattingEnabled = true;
            this.cb_Abonent.Location = new System.Drawing.Point(214, 42);
            this.cb_Abonent.Name = "cb_Abonent";
            this.cb_Abonent.Size = new System.Drawing.Size(181, 24);
            this.cb_Abonent.TabIndex = 1;
            // 
            // cb_Hotline
            // 
            this.cb_Hotline.FormattingEnabled = true;
            this.cb_Hotline.Location = new System.Drawing.Point(23, 42);
            this.cb_Hotline.Name = "cb_Hotline";
            this.cb_Hotline.Size = new System.Drawing.Size(181, 24);
            this.cb_Hotline.TabIndex = 0;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gb_NewAplication);
            this.Controls.Add(this.gb_AddNewHotline);
            this.Controls.Add(this.gb_AddOperator);
            this.Controls.Add(this.gd_AddAbonent);
            this.Controls.Add(this.b_back);
            this.Name = "Form3";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.gd_AddAbonent.ResumeLayout(false);
            this.gd_AddAbonent.PerformLayout();
            this.gb_AddOperator.ResumeLayout(false);
            this.gb_AddOperator.PerformLayout();
            this.gb_AddNewHotline.ResumeLayout(false);
            this.gb_AddNewHotline.PerformLayout();
            this.gb_NewAplication.ResumeLayout(false);
            this.gb_NewAplication.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button b_back;
        private System.Windows.Forms.GroupBox gd_AddAbonent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_Contacts_Abonent;
        private System.Windows.Forms.TextBox tb_PIB_Abonent;
        private System.Windows.Forms.Button b_AddNewAbonent;
        private System.Windows.Forms.GroupBox gb_AddOperator;
        private System.Windows.Forms.Button b_AddOperator;
        private System.Windows.Forms.TextBox tb_numcalls_Operator;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_PIB_Operator;
        private System.Windows.Forms.DateTimePicker dtp_date_employment_Operator;
        private System.Windows.Forms.GroupBox gb_AddNewHotline;
        private System.Windows.Forms.DateTimePicker dtp_Starttime;
        private System.Windows.Forms.TextBox tb_NameHotline;
        private System.Windows.Forms.DateTimePicker dtpEndtime;
        private System.Windows.Forms.Button b_AddNewHotline;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox gb_NewAplication;
        private System.Windows.Forms.Button b_AddNewAplication;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtp_time_finish_aplication;
        private System.Windows.Forms.DateTimePicker dtp_time_start_aplication;
        private System.Windows.Forms.TextBox tb_topic_aplication;
        private System.Windows.Forms.ComboBox cb_Operator;
        private System.Windows.Forms.ComboBox cb_Abonent;
        private System.Windows.Forms.ComboBox cb_Hotline;
        private System.Windows.Forms.DateTimePicker dtp_date_start_aplication;
        private System.Windows.Forms.DateTimePicker dtp_date_finish_aplication;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
    }
}