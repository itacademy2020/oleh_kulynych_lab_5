﻿namespace Oleh_Kulynych_Lab_5
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgv_update_holine = new System.Windows.Forms.DataGridView();
            this.b_update_Hotline = new System.Windows.Forms.Button();
            this.b_back = new System.Windows.Forms.Button();
            this.tb_NewNameHotline = new System.Windows.Forms.TextBox();
            this.dtp_newtimestart_Hotline = new System.Windows.Forms.DateTimePicker();
            this.dtp_newtimefinish_Hotline = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_update_holine)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_newtimefinish_Hotline);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_newtimestart_Hotline);
            this.splitContainer1.Panel1.Controls.Add(this.tb_NewNameHotline);
            this.splitContainer1.Panel1.Controls.Add(this.b_back);
            this.splitContainer1.Panel1.Controls.Add(this.b_update_Hotline);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv_update_holine);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 219;
            this.splitContainer1.TabIndex = 0;
            // 
            // dgv_update_holine
            // 
            this.dgv_update_holine.AllowUserToAddRows = false;
            this.dgv_update_holine.AllowUserToDeleteRows = false;
            this.dgv_update_holine.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_update_holine.Location = new System.Drawing.Point(4, 0);
            this.dgv_update_holine.Name = "dgv_update_holine";
            this.dgv_update_holine.ReadOnly = true;
            this.dgv_update_holine.RowHeadersWidth = 51;
            this.dgv_update_holine.RowTemplate.Height = 24;
            this.dgv_update_holine.Size = new System.Drawing.Size(573, 450);
            this.dgv_update_holine.TabIndex = 0;
            this.dgv_update_holine.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_update_holine_CellClick);
            // 
            // b_update_Hotline
            // 
            this.b_update_Hotline.Location = new System.Drawing.Point(16, 374);
            this.b_update_Hotline.Name = "b_update_Hotline";
            this.b_update_Hotline.Size = new System.Drawing.Size(168, 23);
            this.b_update_Hotline.TabIndex = 0;
            this.b_update_Hotline.Text = "Зберегти зміни";
            this.b_update_Hotline.UseVisualStyleBackColor = true;
            this.b_update_Hotline.Click += new System.EventHandler(this.b_update_Hotline_Click);
            // 
            // b_back
            // 
            this.b_back.Location = new System.Drawing.Point(16, 403);
            this.b_back.Name = "b_back";
            this.b_back.Size = new System.Drawing.Size(168, 23);
            this.b_back.TabIndex = 1;
            this.b_back.Text = "Повернутись назад";
            this.b_back.UseVisualStyleBackColor = true;
            this.b_back.Click += new System.EventHandler(this.b_back_Click);
            // 
            // tb_NewNameHotline
            // 
            this.tb_NewNameHotline.Location = new System.Drawing.Point(16, 48);
            this.tb_NewNameHotline.Name = "tb_NewNameHotline";
            this.tb_NewNameHotline.Size = new System.Drawing.Size(159, 22);
            this.tb_NewNameHotline.TabIndex = 2;
            // 
            // dtp_newtimestart_Hotline
            // 
            this.dtp_newtimestart_Hotline.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtp_newtimestart_Hotline.Location = new System.Drawing.Point(16, 112);
            this.dtp_newtimestart_Hotline.Name = "dtp_newtimestart_Hotline";
            this.dtp_newtimestart_Hotline.ShowUpDown = true;
            this.dtp_newtimestart_Hotline.Size = new System.Drawing.Size(159, 22);
            this.dtp_newtimestart_Hotline.TabIndex = 3;
            // 
            // dtp_newtimefinish_Hotline
            // 
            this.dtp_newtimefinish_Hotline.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtp_newtimefinish_Hotline.Location = new System.Drawing.Point(16, 178);
            this.dtp_newtimefinish_Hotline.Name = "dtp_newtimefinish_Hotline";
            this.dtp_newtimefinish_Hotline.ShowUpDown = true;
            this.dtp_newtimefinish_Hotline.Size = new System.Drawing.Size(159, 22);
            this.dtp_newtimefinish_Hotline.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Нова назва";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Новий час старту";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Новий час кінця";
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form6";
            this.Text = "Form6";
            this.Load += new System.EventHandler(this.Form6_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_update_holine)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp_newtimefinish_Hotline;
        private System.Windows.Forms.DateTimePicker dtp_newtimestart_Hotline;
        private System.Windows.Forms.TextBox tb_NewNameHotline;
        private System.Windows.Forms.Button b_back;
        private System.Windows.Forms.Button b_update_Hotline;
        private System.Windows.Forms.DataGridView dgv_update_holine;
    }
}