﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Oleh_Kulynych_Lab_5
{
    public partial class Form6 : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Kulynych;User ID = Kulynych;Password=Fvcx03756;Trusted_Connection=False;Encrypt=True;";
        private SqlCommandBuilder sqlcombuild = null;
        private SqlConnection sqlConnection = null;
        private SqlDataAdapter sqlDataAdapter = null;
        private DataSet dataSet = null;
        int indexRow;
        public Form6()
        {
            InitializeComponent();
        }

        private void b_back_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            sqlConnection = new SqlConnection(sqlconnectionstring);
            sqlConnection.Open();
            LoadData();
            sqlConnection.Close();
        }
        private void LoadData()
        {
            try
            {
                sqlDataAdapter = new SqlDataAdapter("SELECT * FROM [dbo].[ListHotline];", sqlConnection);
                sqlcombuild = new SqlCommandBuilder(sqlDataAdapter);
                sqlcombuild.GetUpdateCommand();

                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "ListHotline");
                dgv_update_holine.DataSource = dataSet.Tables["ListHotline"];

            }
            catch (Exception ex)
            {
                string message = "Помилка при виведенні записів з таблиці ListHotline.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }

        private void dgv_update_holine_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                indexRow = e.RowIndex;
                DataGridViewRow row = dgv_update_holine.Rows[indexRow];
                tb_NewNameHotline.Text = row.Cells[1].Value.ToString();
                dtp_newtimestart_Hotline.Text = row.Cells[2].Value.ToString();
                dtp_newtimefinish_Hotline.Text = row.Cells[3].Value.ToString();
            }
            catch (Exception exp)
            {
                string message = "Помилка. Виберіть конкретну лінію. ";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }

        private void b_update_Hotline_Click(object sender, EventArgs e)
        {
            if(tb_NewNameHotline.Text.ToString() != "")
            {
                try
                {
                    sqlConnection.Open();
                    DataGridViewRow newDataRow = dgv_update_holine.Rows[indexRow];
                    newDataRow.Cells[1].Value = tb_NewNameHotline.Text;
                    newDataRow.Cells[2].Value = dtp_newtimestart_Hotline.Text;
                    newDataRow.Cells[3].Value = dtp_newtimefinish_Hotline.Text;
                    SqlCommand command = new SqlCommand("UPDATE [dbo].[ListHotline] SET [name_hotline] = '" + tb_NewNameHotline.Text.ToString() + "', [start_date] = '" + dtp_newtimestart_Hotline.Value.TimeOfDay.ToString() + "', [finish_date] = '" + dtp_newtimefinish_Hotline.Value.TimeOfDay.ToString() + "' WHERE [ID] = " + newDataRow.Cells[0].Value + ";", sqlConnection);
                    command.ExecuteNonQuery();
                    sqlConnection.Close();

                    string message = "Лінія змінена успішно.";
                    MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
                    dgv_update_holine.Refresh();
                }
                catch (Exception ex)
                {
                    string message = "Помилка при редагуванні лінії.";
                    MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                }
            }
            else
            {
                string message = "Виберіть лінію.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }
    }
}
