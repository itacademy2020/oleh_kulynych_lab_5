﻿namespace Oleh_Kulynych_Lab_5
{
    partial class Form10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.b_active = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_third = new System.Windows.Forms.DateTimePicker();
            this.b_back = new System.Windows.Forms.Button();
            this.dgv_third = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_third)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.b_active);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_third);
            this.splitContainer1.Panel1.Controls.Add(this.b_back);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv_third);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 186;
            this.splitContainer1.TabIndex = 0;
            // 
            // b_active
            // 
            this.b_active.Location = new System.Drawing.Point(16, 379);
            this.b_active.Name = "b_active";
            this.b_active.Size = new System.Drawing.Size(154, 23);
            this.b_active.TabIndex = 3;
            this.b_active.Text = "Скласти звіт";
            this.b_active.UseVisualStyleBackColor = true;
            this.b_active.Click += new System.EventHandler(this.b_active_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Виберіть дату";
            // 
            // dtp_third
            // 
            this.dtp_third.Location = new System.Drawing.Point(13, 81);
            this.dtp_third.Name = "dtp_third";
            this.dtp_third.Size = new System.Drawing.Size(157, 22);
            this.dtp_third.TabIndex = 1;
            // 
            // b_back
            // 
            this.b_back.Location = new System.Drawing.Point(13, 415);
            this.b_back.Name = "b_back";
            this.b_back.Size = new System.Drawing.Size(157, 23);
            this.b_back.TabIndex = 0;
            this.b_back.Text = "Повернутись назад";
            this.b_back.UseVisualStyleBackColor = true;
            this.b_back.Click += new System.EventHandler(this.b_back_Click);
            // 
            // dgv_third
            // 
            this.dgv_third.AllowUserToAddRows = false;
            this.dgv_third.AllowUserToDeleteRows = false;
            this.dgv_third.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_third.Location = new System.Drawing.Point(4, 4);
            this.dgv_third.Name = "dgv_third";
            this.dgv_third.ReadOnly = true;
            this.dgv_third.RowHeadersWidth = 51;
            this.dgv_third.RowTemplate.Height = 24;
            this.dgv_third.Size = new System.Drawing.Size(606, 446);
            this.dgv_third.TabIndex = 0;
            // 
            // Form10
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form10";
            this.Text = "Form10";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_third)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgv_third;
        private System.Windows.Forms.Button b_back;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp_third;
        private System.Windows.Forms.Button b_active;
    }
}