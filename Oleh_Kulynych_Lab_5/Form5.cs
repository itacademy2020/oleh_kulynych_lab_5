﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Oleh_Kulynych_Lab_5
{
    public partial class Form5 : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Kulynych;User ID = Kulynych;Password=Fvcx03756;Trusted_Connection=False;Encrypt=True;";
        private SqlCommandBuilder sqlcombuild = null;
        private SqlConnection sqlConnection = null;
        private SqlDataAdapter sqlDataAdapter = null;
        private DataSet dataSet = null;
        int indexRow;
        public Form5()
        {
            InitializeComponent();
        }

        private void b_back_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            sqlConnection = new SqlConnection(sqlconnectionstring);
            sqlConnection.Open();
            LoadData();
            sqlConnection.Close();
        }
        private void LoadData()
        {
            try
            {
                sqlDataAdapter = new SqlDataAdapter("SELECT * FROM [dbo].[Operator];", sqlConnection);
                sqlcombuild = new SqlCommandBuilder(sqlDataAdapter);
                sqlcombuild.GetUpdateCommand();

                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "Operator");
                dgv_update_operator.DataSource = dataSet.Tables["Operator"];

            }
            catch (Exception ex)
            {
                string message = "Помилка при виведенні записів з таблиці Operator.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }

        private void dgv_update_operator_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                indexRow = e.RowIndex;
                DataGridViewRow row = dgv_update_operator.Rows[indexRow];
                tb_NewPIB_Operator.Text = row.Cells[1].Value.ToString();
                dtp_newdateemp_Operator.Text = row.Cells[2].Value.ToString();
                tb_newnumcall_Operator.Text = row.Cells[3].Value.ToString();
            }
            catch (Exception exp)
            {
                string message = "Помилка. Виберіть конкретного оператора. ";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }

        private void b_update_operator_Click(object sender, EventArgs e)
        {
            if (tb_NewPIB_Operator.Text.ToString() != "")
            {
                if(tb_newnumcall_Operator.Text.ToString() != "")
                {
                    try
                    {
                        sqlConnection.Open();
                        DataGridViewRow newDataRow = dgv_update_operator.Rows[indexRow];
                        newDataRow.Cells[1].Value = tb_NewPIB_Operator.Text;
                        newDataRow.Cells[2].Value = dtp_newdateemp_Operator.Text;
                        newDataRow.Cells[3].Value = tb_newnumcall_Operator.Text;
                        SqlCommand command = new SqlCommand("UPDATE [dbo].[Operator] SET [Operator_PIB] = '" + tb_NewPIB_Operator.Text + "', [date_employment] = '" + dtp_newdateemp_Operator.Value.Date.ToString() + "', [number_calls] = " + tb_newnumcall_Operator.Text + " WHERE [ID] = " + newDataRow.Cells[0].Value + ";", sqlConnection);
                        command.ExecuteNonQuery();
                        sqlConnection.Close();

                        string message = "Оператор змінений успішно.";
                        MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
                        dgv_update_operator.Refresh();


                    }
                    catch (Exception ex)
                    {
                        string message = "Помилка при редагуванні оператора.";
                        MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    string message = "Виберіть оператора.";
                    MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                }
            }
            else
            {
                string message = "Виберіть оператора.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }

        private void tb_newnumcall_Operator_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }


    }
}
