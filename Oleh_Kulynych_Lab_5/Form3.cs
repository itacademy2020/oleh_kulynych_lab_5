﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Oleh_Kulynych_Lab_5
{
    public partial class Form3 : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Kulynych;User ID = Kulynych;Password=Fvcx03756;Trusted_Connection=False;Encrypt=True;";
        public Form3()
        {
            InitializeComponent();
        }

        private void b_back_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }

        private void b_AddNewAbonent_Click(object sender, EventArgs e)
        {
            if(tb_PIB_Abonent.Text.ToString() != "")
            {
                if(tb_Contacts_Abonent.Text.ToString() != "")
                {
                    SqlConnection con = new SqlConnection(sqlconnectionstring);
                    try
                    {
                        con.Open();
                        SqlCommand command = new SqlCommand("INSERT INTO [dbo].[Abonent] ([PIB],[Contacts]) VALUES('" + tb_PIB_Abonent.Text.ToString() + "', '" + tb_Contacts_Abonent.Text.ToString() + "');", con);
                        command.ExecuteNonQuery();

                        string message = "Абонент доданий успішно.";
                        MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
                    }
                    catch (Exception exp)
                    {
                        string message = "Виникла помилка при додаванні абонента.";
                        MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
                else
                {
                    string message = "Введіть значення в поле Contacts.";
                    MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                }
            }
            else
            {
                string message = "Введіть значення в поле PIB.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);               
            }
        }

        private void b_AddOperator_Click(object sender, EventArgs e)
        {
            if (tb_PIB_Operator.Text.ToString() != "")
            {
                if(tb_numcalls_Operator.Text.ToString()!="")
                {
                    SqlConnection con = new SqlConnection(sqlconnectionstring);
                    try
                    {
                        con.Open();
                        SqlCommand command = new SqlCommand("INSERT INTO [dbo].Operator([Operator_PIB],[date_employment],[number_calls]) VALUES('" + tb_PIB_Operator.Text.ToString() + "','" + dtp_date_employment_Operator.Value.Date.ToString("yyyy-MM-dd") + "', '" + tb_numcalls_Operator.Text.ToString() + "');", con);
                        command.ExecuteNonQuery();

                        string message = "Оператор доданий успішно.";
                        MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
                    }
                    catch (Exception exp)
                    {
                        string message = "Виникла помилка при додаванні оператора.";
                        MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
                else
                {
                    string message = "Введіть значення в поле 'Число дзвінків'.";
                    MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                }
            }
            else
            {
                string message = "Введіть значення в поле PIB.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }

        private void b_AddNewHotline_Click(object sender, EventArgs e)
        {
            if(tb_NameHotline.Text.ToString() != "")
            {
                SqlConnection con = new SqlConnection(sqlconnectionstring);
                try
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("INSERT INTO [dbo].[ListHotline]([name_hotline],[start_date],[finish_date]) VALUES ('" + tb_NameHotline.Text.ToString() + "','" + dtp_Starttime.Value.TimeOfDay.ToString() + "','" + dtpEndtime.Value.TimeOfDay.ToString() + "');", con);
                    command.ExecuteNonQuery();
                    string message = "Лінія додана успішно.";
                    MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
                }
                catch (Exception exp)
                {
                    string message = "Виникла помилка при додаванні гарячої лінії.";
                    MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                }
                finally
                {
                    con.Close();
                }
            }
            else
            {
                string message = "Введіть значення в поле 'Назва лінії'.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlCommand command_hotline = new SqlCommand("SELECT [ID],[name_hotline], [start_date], [finish_date] FROM [ListHotline];",con);
            SqlDataReader dataReader_hotline = command_hotline.ExecuteReader();

            cb_Hotline.Items.Clear();

            while(dataReader_hotline.Read())
            {
                HotLine hotLine = new HotLine();
                hotLine.ID = (int)dataReader_hotline[0];
                hotLine.Name = dataReader_hotline[1].ToString();
                cb_Hotline.Items.Add(hotLine);
                cb_Hotline.DisplayMember = "Name";
            }
            con.Close();

            con.Open();
            SqlCommand command_abonent = new SqlCommand("SELECT [ID], [PIB], [Contacts] FROM [Abonent];",con);
            SqlDataReader dataReader_abonent = command_abonent.ExecuteReader();

            cb_Abonent.Items.Clear();
            while(dataReader_abonent.Read())
            {
                Abonent abonent = new Abonent();
                abonent.ID = (int)dataReader_abonent[0];
                abonent.PIB = dataReader_abonent[1].ToString();
                cb_Abonent.Items.Add(abonent);
                cb_Abonent.DisplayMember = "PIB";
            }
            con.Close();

            con.Open();

            SqlCommand command_operator = new SqlCommand("SELECT [ID],[Operator_PIB], [date_employment], [number_calls] FROM [Operator];", con);
            SqlDataReader dataReader_operator = command_operator.ExecuteReader();

            cb_Operator.Items.Clear();
            while(dataReader_operator.Read())
            {
                Operator oper = new Operator();
                oper.ID = (int)dataReader_operator[0];
                oper.PIB = dataReader_operator[1].ToString();
                cb_Operator.Items.Add(oper);
                cb_Operator.DisplayMember = "PIB";
            }
            con.Close();
            
        }

        private void b_AddNewAplication_Click(object sender, EventArgs e)
        {
            if(cb_Hotline.Text.ToString() != "")
            {
                if(cb_Operator.Text.ToString() != "")
                {
                    if(cb_Abonent.Text.ToString() != "")
                    {
                        if (tb_topic_aplication.Text.ToString() != "")
                        {

                            HotLine hotLine = (HotLine)cb_Hotline.SelectedItem;
                            Abonent abonent = (Abonent)cb_Abonent.SelectedItem;
                            Operator oper = (Operator)cb_Operator.SelectedItem;

                            SqlConnection con = new SqlConnection(sqlconnectionstring);
                            try
                            {
                                con.Open();
                                SqlCommand command = new SqlCommand("INSERT INTO [dbo].[Aplication]([Hotline_ID],[Abonent_ID],[Operator_ID],[topic_aplication],[datestart_aplication],[datefinish_aplication],[timestart_aplication],[timefinish_aplication]) VALUES ('" + hotLine.ID.ToString() + "','" + abonent.ID.ToString() + "','" + oper.ID.ToString() + "','" + tb_topic_aplication.Text.ToString() + "','" + dtp_date_start_aplication.Value.Date.ToString("yyyy-MM-dd") + "','" + dtp_date_finish_aplication.Value.Date.ToString("yyyy-MM-dd") + "','" + dtp_time_start_aplication.Value.TimeOfDay.ToString() + "','" + dtp_time_finish_aplication.Value.TimeOfDay.ToString() + "');", con);
                                command.ExecuteNonQuery();
                                string message = "Звернення додане успішно.";
                                MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
                            }
                            catch (Exception exp)
                            {
                                string message = "Виникла помилка при додаванні звернення.";
                                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                            }
                            finally
                            {
                                con.Close();
                            }
                        }
                        else
                        {
                            string message = "Введіть значення в поле 'Тема звернення'.";
                            MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        string message = "Виберіть абонента.";
                        MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    string message = "Виберіть оператора.";
                    MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                }
            }
            else
            {
                string message = "Виберіть лінію.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }

        private void tb_Contacts_Abonent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void tb_numcalls_Operator_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }
    }
}
