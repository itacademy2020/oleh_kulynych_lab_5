﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Oleh_Kulynych_Lab_5
{
    public partial class Form7 : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Kulynych;User ID = Kulynych;Password=Fvcx03756;Trusted_Connection=False;Encrypt=True;";
        private SqlConnection sqlConnection = null;
        private SqlDataAdapter sqlDataAdapter = null;
        private DataSet dataSet = null;
        int indexRow;
        public Form7()
        {
            InitializeComponent();
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            sqlConnection = new SqlConnection(sqlconnectionstring);
            sqlConnection.Open();

            SqlCommand command_hotline = new SqlCommand("SELECT [ID],[name_hotline], [start_date], [finish_date] FROM [ListHotline];", sqlConnection);
            SqlDataReader dataReader_hotline = command_hotline.ExecuteReader();

            cb_new_hotline.Items.Clear();

            while (dataReader_hotline.Read())
            {
                HotLine hotLine = new HotLine();
                hotLine.ID = (int)dataReader_hotline[0];
                hotLine.Name = dataReader_hotline[1].ToString();
                cb_new_hotline.Items.Add(hotLine);
                cb_new_hotline.DisplayMember = "Name";
            }
            sqlConnection.Close();

            sqlConnection.Open();
            SqlCommand command_abonent = new SqlCommand("SELECT [ID], [PIB], [Contacts] FROM [Abonent];", sqlConnection);
            SqlDataReader dataReader_abonent = command_abonent.ExecuteReader();

            cb_new_abonent.Items.Clear();
            while (dataReader_abonent.Read())
            {
                Abonent abonent = new Abonent();
                abonent.ID = (int)dataReader_abonent[0];
                abonent.PIB = dataReader_abonent[1].ToString();
                cb_new_abonent.Items.Add(abonent);
                cb_new_abonent.DisplayMember = "PIB";
            }
            sqlConnection.Close();

            sqlConnection.Open();

            SqlCommand command_operator = new SqlCommand("SELECT [ID],[Operator_PIB], [date_employment], [number_calls] FROM [Operator];", sqlConnection);
            SqlDataReader dataReader_operator = command_operator.ExecuteReader();

            cb_new_operator.Items.Clear();
            while (dataReader_operator.Read())
            {
                Operator oper = new Operator();
                oper.ID = (int)dataReader_operator[0];
                oper.PIB = dataReader_operator[1].ToString();
                cb_new_operator.Items.Add(oper);
                cb_new_operator.DisplayMember = "PIB";
            }

            
            sqlConnection.Close();

            
            LoadData();
            

        }
        private void LoadData()
        {
            try
            {
                sqlConnection.Open();

                sqlDataAdapter = new SqlDataAdapter("SELECT APL.ID, A.PIB, O.Operator_PIB , LH.name_hotline , topic_aplication, datestart_aplication, datefinish_aplication, timestart_aplication, timefinish_aplication FROM [dbo].[Aplication] APL INNER JOIN [dbo].Abonent A ON APL.Abonent_ID = A.ID INNER JOIN [dbo].[Operator] O ON APL.Operator_ID = O.ID INNER JOIN [dbo].ListHotline LH ON APL.Hotline_ID = LH.ID;", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "Aplication");
                dgv_update_aplication.DataSource = dataSet.Tables["Aplication"];
                dgv_update_aplication.Refresh();

                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                string message = "Помилка при виведенні записів з таблиці Aplication.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }
        private void b_back_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }

        private void dgv_update_aplication_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                indexRow = e.RowIndex;
                DataGridViewRow row = dgv_update_aplication.Rows[indexRow];
                cb_new_hotline.Text = row.Cells[3].Value.ToString(); ;
                cb_new_abonent.Text = row.Cells[1].Value.ToString();
                cb_new_operator.Text = row.Cells[2].Value.ToString();
                tb_newtopic_aplication.Text = row.Cells[4].Value.ToString();
                dtp_newstartaplication.Text = row.Cells[5].Value.ToString();
                dtp_newfinishaplication.Text = row.Cells[6].Value.ToString();
                dtp_newtimestartaplication.Text = row.Cells[7].Value.ToString();
                dtp_newtimefinishaplication.Text = row.Cells[8].Value.ToString();
            }
            catch (Exception exp)
            {
                string message = "Помилка. Виберіть конкретне звернення. ";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }

        private void b_update_aplication_Click(object sender, EventArgs e)
        {
            if(cb_new_hotline.Text.ToString() != "")
            {
                if(cb_new_operator.Text.ToString() != "")
                {
                    if(cb_new_abonent.Text.ToString() != "")
                    {
                        if(tb_newtopic_aplication.Text.ToString() != "")
                        {
                            try
                            {
                                sqlConnection.Open();
                                HotLine hotLine = (HotLine)cb_new_hotline.SelectedItem;
                                Abonent abonent = (Abonent)cb_new_abonent.SelectedItem;
                                Operator oper = (Operator)cb_new_operator.SelectedItem;

                                DataGridViewRow newDataRow = dgv_update_aplication.Rows[indexRow];
                                newDataRow.Cells[1].Value = cb_new_abonent.Text;
                                newDataRow.Cells[2].Value = cb_new_operator.Text;
                                newDataRow.Cells[3].Value = cb_new_hotline.Text;
                                newDataRow.Cells[4].Value = tb_newtopic_aplication.Text;
                                newDataRow.Cells[5].Value = dtp_newstartaplication.Text;
                                newDataRow.Cells[6].Value = dtp_newfinishaplication.Text;
                                newDataRow.Cells[7].Value = dtp_newtimestartaplication.Text;
                                newDataRow.Cells[8].Value = dtp_newtimefinishaplication.Text;


                                SqlCommand command = new SqlCommand("UPDATE [dbo].[Aplication] SET [Hotline_ID] = '" + hotLine.ID.ToString() + "', [Abonent_ID] = '" + abonent.ID.ToString() + "', [Operator_ID] = '" + oper.ID.ToString() + "', [topic_aplication] = '" + tb_newtopic_aplication.Text.ToString() + "', [datestart_aplication] = '" + dtp_newstartaplication.Value.Date.ToString("yyyy-MM-dd") + "', [datefinish_aplication] = '" + dtp_newfinishaplication.Value.Date.ToString("yyyy-MM-dd") + "', [timestart_aplication]='" + dtp_newtimestartaplication.Value.TimeOfDay.ToString() + "', [timefinish_aplication]='" + dtp_newtimefinishaplication.Value.TimeOfDay.ToString() + "' WHERE [ID] = '" + newDataRow.Cells[0].Value + "';", sqlConnection);
                                command.ExecuteNonQuery();
                                sqlConnection.Close();

                                string message = "Звернення змінено успішно.";
                                MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
                                dgv_update_aplication.Refresh();
                            }
                            catch (Exception ex)
                            {
                                string message = "Помилка при редагуванні звернення.";
                                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                            }
                        }
                        else
                        {
                            string message = "Виберіть звернення.";
                            MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        string message = "Виберіть звернення.";
                        MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    string message = "Виберіть звернення.";
                    MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                }
            }
            else
            {
                string message = "Виберіть звернення.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }
    }
}
