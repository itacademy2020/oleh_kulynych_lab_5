﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Oleh_Kulynych_Lab_5
{
    public partial class Form2 : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Kulynych;User ID = Kulynych;Password=Fvcx03756;Trusted_Connection=False;Encrypt=True;";
        public Form2()
        {
            InitializeComponent();
        }

        private void b_back_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }

 

        private void b_output_abonent_Click(object sender, EventArgs e)
        {
            
            try
            {
                SqlConnection con = new SqlConnection(sqlconnectionstring);
                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM [dbo].[Abonent];", con);
                DataSet DS = new DataSet();
                DA.Fill(DS, "Abonent");
                dgvResult.DataSource = DS.Tables["Abonent"];
                dgvResult.Refresh();
                con.Close();
            }
            catch(Exception ex)
            {
                string message = "Помилка при виведенні записів таблиці Abonent.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }            
        }

        private void b_output_operator_Click(object sender, EventArgs e)
        {
            
            try
            {
                SqlConnection con = new SqlConnection(sqlconnectionstring);
                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM [dbo].[Operator];", con);
                DataSet DS = new DataSet();
                DA.Fill(DS, "Operator");
                dgvResult.DataSource = DS.Tables["Operator"];
                dgvResult.Refresh();

                con.Close();
            }
            catch(Exception ex)
            {
                string message = "Помилка при виведенні записів таблиці Operator.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }          
        }

        private void b_output_hotline_Click(object sender, EventArgs e)
        {
           
            try
            {
                SqlConnection con = new SqlConnection(sqlconnectionstring);
                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM [dbo].[ListHotline];", con);
                DataSet DS = new DataSet();
                DA.Fill(DS, "ListHotline");
                dgvResult.DataSource = DS.Tables["ListHotline"];
                dgvResult.Refresh();

                con.Close();
            }
            catch(Exception ex)
            {
                string message = "Помилка при виведенні записів таблиці ListHotline.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
            
        }

        private void b_output_aplication_Click(object sender, EventArgs e)
        {
           
            try
            {
                SqlConnection con = new SqlConnection(sqlconnectionstring);
                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter("SELECT APL.ID, A.PIB, O.Operator_PIB , LH.name_hotline , topic_aplication, datestart_aplication, datefinish_aplication, timestart_aplication, timefinish_aplication FROM [dbo].[Aplication] APL INNER JOIN [dbo].Abonent A ON APL.Abonent_ID = A.ID INNER JOIN [dbo].[Operator] O ON APL.Operator_ID = O.ID INNER JOIN [dbo].ListHotline LH ON APL.Hotline_ID = LH.ID;", con);
                DataSet DS = new DataSet();
                DA.Fill(DS, "Aplication");
                dgvResult.DataSource = DS.Tables["Aplication"];
                dgvResult.Refresh();

                con.Close();
            }
            catch(Exception ex)
            {
                string message = "Помилка при виведенні записів таблиці Aplication.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
            
        }    

    }
}
