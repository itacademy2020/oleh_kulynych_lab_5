﻿namespace Oleh_Kulynych_Lab_5
{
    partial class Form9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.b_back = new System.Windows.Forms.Button();
            this.dgv_second = new System.Windows.Forms.DataGridView();
            this.cb_operator = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.b_active = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_second)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.b_active);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.cb_operator);
            this.splitContainer1.Panel1.Controls.Add(this.b_back);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv_second);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 182;
            this.splitContainer1.TabIndex = 0;
            // 
            // b_back
            // 
            this.b_back.Location = new System.Drawing.Point(13, 415);
            this.b_back.Name = "b_back";
            this.b_back.Size = new System.Drawing.Size(152, 23);
            this.b_back.TabIndex = 0;
            this.b_back.Text = "Повернутись назад";
            this.b_back.UseVisualStyleBackColor = true;
            this.b_back.Click += new System.EventHandler(this.b_back_Click);
            // 
            // dgv_second
            // 
            this.dgv_second.AllowUserToAddRows = false;
            this.dgv_second.AllowUserToDeleteRows = false;
            this.dgv_second.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_second.Location = new System.Drawing.Point(4, 4);
            this.dgv_second.Name = "dgv_second";
            this.dgv_second.ReadOnly = true;
            this.dgv_second.RowHeadersWidth = 51;
            this.dgv_second.RowTemplate.Height = 24;
            this.dgv_second.Size = new System.Drawing.Size(609, 446);
            this.dgv_second.TabIndex = 0;
            // 
            // cb_operator
            // 
            this.cb_operator.FormattingEnabled = true;
            this.cb_operator.Location = new System.Drawing.Point(13, 33);
            this.cb_operator.Name = "cb_operator";
            this.cb_operator.Size = new System.Drawing.Size(166, 24);
            this.cb_operator.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Оператор";
            // 
            // b_active
            // 
            this.b_active.Location = new System.Drawing.Point(16, 341);
            this.b_active.Name = "b_active";
            this.b_active.Size = new System.Drawing.Size(132, 23);
            this.b_active.TabIndex = 5;
            this.b_active.Text = "Скласти звіт";
            this.b_active.UseVisualStyleBackColor = true;
            this.b_active.Click += new System.EventHandler(this.b_active_Click);
            // 
            // Form9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form9";
            this.Text = "Form9";
            this.Load += new System.EventHandler(this.Form9_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_second)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgv_second;
        private System.Windows.Forms.Button b_back;
        private System.Windows.Forms.Button b_active;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_operator;
    }
}