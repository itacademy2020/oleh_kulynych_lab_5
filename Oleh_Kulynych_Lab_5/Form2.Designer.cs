﻿namespace Oleh_Kulynych_Lab_5
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_output_abonent = new System.Windows.Forms.Button();
            this.b_output_operator = new System.Windows.Forms.Button();
            this.b_output_aplication = new System.Windows.Forms.Button();
            this.b_output_hotline = new System.Windows.Forms.Button();
            this.b_back = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            this.SuspendLayout();
            // 
            // b_output_abonent
            // 
            this.b_output_abonent.Location = new System.Drawing.Point(12, 20);
            this.b_output_abonent.Name = "b_output_abonent";
            this.b_output_abonent.Size = new System.Drawing.Size(143, 51);
            this.b_output_abonent.TabIndex = 0;
            this.b_output_abonent.Text = "Вивід абонентів";
            this.b_output_abonent.UseVisualStyleBackColor = true;
            this.b_output_abonent.Click += new System.EventHandler(this.b_output_abonent_Click);
            // 
            // b_output_operator
            // 
            this.b_output_operator.Location = new System.Drawing.Point(13, 77);
            this.b_output_operator.Name = "b_output_operator";
            this.b_output_operator.Size = new System.Drawing.Size(142, 50);
            this.b_output_operator.TabIndex = 1;
            this.b_output_operator.Text = "Вивід операторів";
            this.b_output_operator.UseVisualStyleBackColor = true;
            this.b_output_operator.Click += new System.EventHandler(this.b_output_operator_Click);
            // 
            // b_output_aplication
            // 
            this.b_output_aplication.Location = new System.Drawing.Point(13, 133);
            this.b_output_aplication.Name = "b_output_aplication";
            this.b_output_aplication.Size = new System.Drawing.Size(143, 50);
            this.b_output_aplication.TabIndex = 2;
            this.b_output_aplication.Text = "Вивід звернень";
            this.b_output_aplication.UseVisualStyleBackColor = true;
            this.b_output_aplication.Click += new System.EventHandler(this.b_output_aplication_Click);
            // 
            // b_output_hotline
            // 
            this.b_output_hotline.Location = new System.Drawing.Point(13, 189);
            this.b_output_hotline.Name = "b_output_hotline";
            this.b_output_hotline.Size = new System.Drawing.Size(144, 49);
            this.b_output_hotline.TabIndex = 3;
            this.b_output_hotline.Text = "Вивід гарячих ліній";
            this.b_output_hotline.UseVisualStyleBackColor = true;
            this.b_output_hotline.Click += new System.EventHandler(this.b_output_hotline_Click);
            // 
            // b_back
            // 
            this.b_back.Location = new System.Drawing.Point(13, 409);
            this.b_back.Name = "b_back";
            this.b_back.Size = new System.Drawing.Size(144, 30);
            this.b_back.TabIndex = 4;
            this.b_back.Text = "Повернутись назад";
            this.b_back.UseVisualStyleBackColor = true;
            this.b_back.Click += new System.EventHandler(this.b_back_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, -1);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.b_output_abonent);
            this.splitContainer1.Panel1.Controls.Add(this.b_back);
            this.splitContainer1.Panel1.Controls.Add(this.b_output_operator);
            this.splitContainer1.Panel1.Controls.Add(this.b_output_hotline);
            this.splitContainer1.Panel1.Controls.Add(this.b_output_aplication);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvResult);
            this.splitContainer1.Size = new System.Drawing.Size(801, 452);
            this.splitContainer1.SplitterDistance = 183;
            this.splitContainer1.TabIndex = 5;
            // 
            // dgvResult
            // 
            this.dgvResult.AllowUserToAddRows = false;
            this.dgvResult.AllowUserToDeleteRows = false;
            this.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResult.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.dgvResult.Location = new System.Drawing.Point(-7, 0);
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.RowHeadersWidth = 51;
            this.dgvResult.RowTemplate.Height = 24;
            this.dgvResult.Size = new System.Drawing.Size(621, 452);
            this.dgvResult.TabIndex = 5;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button b_output_abonent;
        private System.Windows.Forms.Button b_output_operator;
        private System.Windows.Forms.Button b_output_aplication;
        private System.Windows.Forms.Button b_output_hotline;
        private System.Windows.Forms.Button b_back;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvResult;
    }
}