﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Oleh_Kulynych_Lab_5
{
    public partial class Form8 : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Kulynych;User ID = Kulynych;Password=Fvcx03756;Trusted_Connection=False;Encrypt=True;";
        public Form8()
        {
            InitializeComponent();
        }

        private void b_back_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }

        private void Form8_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlCommand command_operator = new SqlCommand("SELECT [ID],[Operator_PIB], [date_employment], [number_calls] FROM [Operator];", con);
            SqlDataReader dataReader_operator = command_operator.ExecuteReader();

            cb_operator.Items.Clear();
            while (dataReader_operator.Read())
            {
                Operator oper = new Operator();
                oper.ID = (int)dataReader_operator[0];
                oper.PIB = dataReader_operator[1].ToString();
                cb_operator.Items.Add(oper);
                cb_operator.DisplayMember = "PIB";
            }
            con.Close();
        }

        private void b_active_Click(object sender, EventArgs e)
        {
            Operator oper = (Operator)cb_operator.SelectedItem;
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            try
            {
                con.Open();           
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("SELECT  [ListHotline].[name_hotline],[Abonent].[PIB],[Aplication].[topic_aplication], [Aplication].[datestart_aplication], [Operator].[Operator_PIB] FROM[dbo].[Aplication] LEFT JOIN[dbo].[ListHotline] ON[Aplication].[Hotline_ID] = [ListHotline].[ID] LEFT JOIN[dbo].[Operator] ON [Aplication].[Operator_ID] = [Operator].[ID] LEFT JOIN [dbo].[Abonent] ON [Aplication].[Abonent_ID] = [Abonent].[ID] WHERE datestart_aplication >= '" + dtp_firstdate.Value.Date.ToString("yyyy-MM-dd") + "' AND datestart_aplication <= '" + dtp_seconddate.Value.Date.ToString("yyyy-MM-dd") + "' AND [Operator_ID] = '"+ oper.ID.ToString()+"'", con);
                DataSet dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "Aplication");
                dgv_first.DataSource = dataSet.Tables["Aplication"];
                dgv_first.Refresh();
                string message = "Звіт складений успішно.";
                MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
            }
            catch (Exception exp)
            {
                string message = "Виникла помилка при складанні звіту.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
            finally
            {
                con.Close();
            }
        }

        private void dtp_firstdate_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
