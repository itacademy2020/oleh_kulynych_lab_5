﻿namespace Oleh_Kulynych_Lab_5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_main_input = new System.Windows.Forms.Button();
            this.b_main_output = new System.Windows.Forms.Button();
            this.b_main_update_abonent = new System.Windows.Forms.Button();
            this.b_main_update_operator = new System.Windows.Forms.Button();
            this.b_main_update_hotline = new System.Windows.Forms.Button();
            this.b_main_update_aplication = new System.Windows.Forms.Button();
            this.b_first = new System.Windows.Forms.Button();
            this.b_second = new System.Windows.Forms.Button();
            this.b_third = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // b_main_input
            // 
            this.b_main_input.Location = new System.Drawing.Point(549, 12);
            this.b_main_input.Name = "b_main_input";
            this.b_main_input.Size = new System.Drawing.Size(220, 67);
            this.b_main_input.TabIndex = 0;
            this.b_main_input.Text = "Ввід даних в таблиці";
            this.b_main_input.UseVisualStyleBackColor = true;
            this.b_main_input.Click += new System.EventHandler(this.b_main_input_Click);
            // 
            // b_main_output
            // 
            this.b_main_output.Location = new System.Drawing.Point(24, 12);
            this.b_main_output.Name = "b_main_output";
            this.b_main_output.Size = new System.Drawing.Size(220, 67);
            this.b_main_output.TabIndex = 1;
            this.b_main_output.Text = "Вивід даних з таблиць";
            this.b_main_output.UseVisualStyleBackColor = true;
            this.b_main_output.Click += new System.EventHandler(this.b_main_output_Click);
            // 
            // b_main_update_abonent
            // 
            this.b_main_update_abonent.Location = new System.Drawing.Point(24, 98);
            this.b_main_update_abonent.Name = "b_main_update_abonent";
            this.b_main_update_abonent.Size = new System.Drawing.Size(270, 67);
            this.b_main_update_abonent.TabIndex = 2;
            this.b_main_update_abonent.Text = "Оновлення даних в таблиці Abonent\r\n";
            this.b_main_update_abonent.UseVisualStyleBackColor = true;
            this.b_main_update_abonent.Click += new System.EventHandler(this.b_main_update_abonent_Click);
            // 
            // b_main_update_operator
            // 
            this.b_main_update_operator.Location = new System.Drawing.Point(499, 98);
            this.b_main_update_operator.Name = "b_main_update_operator";
            this.b_main_update_operator.Size = new System.Drawing.Size(270, 67);
            this.b_main_update_operator.TabIndex = 3;
            this.b_main_update_operator.Text = "Оновлення даних в таблиці Operator";
            this.b_main_update_operator.UseVisualStyleBackColor = true;
            this.b_main_update_operator.Click += new System.EventHandler(this.b_main_update_operator_Click);
            // 
            // b_main_update_hotline
            // 
            this.b_main_update_hotline.Location = new System.Drawing.Point(24, 194);
            this.b_main_update_hotline.Name = "b_main_update_hotline";
            this.b_main_update_hotline.Size = new System.Drawing.Size(270, 67);
            this.b_main_update_hotline.TabIndex = 4;
            this.b_main_update_hotline.Text = "Оновлення даних в таблиці ListHotline";
            this.b_main_update_hotline.UseVisualStyleBackColor = true;
            this.b_main_update_hotline.Click += new System.EventHandler(this.b_main_update_hotline_Click);
            // 
            // b_main_update_aplication
            // 
            this.b_main_update_aplication.Location = new System.Drawing.Point(499, 194);
            this.b_main_update_aplication.Name = "b_main_update_aplication";
            this.b_main_update_aplication.Size = new System.Drawing.Size(270, 67);
            this.b_main_update_aplication.TabIndex = 5;
            this.b_main_update_aplication.Text = "Оновлення даних в таблиці Aplication";
            this.b_main_update_aplication.UseVisualStyleBackColor = true;
            this.b_main_update_aplication.Click += new System.EventHandler(this.b_main_update_aplication_Click);
            // 
            // b_first
            // 
            this.b_first.Location = new System.Drawing.Point(24, 309);
            this.b_first.Name = "b_first";
            this.b_first.Size = new System.Drawing.Size(155, 49);
            this.b_first.TabIndex = 6;
            this.b_first.Text = "Перший звіт";
            this.b_first.UseVisualStyleBackColor = true;
            this.b_first.Click += new System.EventHandler(this.b_first_Click);
            // 
            // b_second
            // 
            this.b_second.Location = new System.Drawing.Point(328, 309);
            this.b_second.Name = "b_second";
            this.b_second.Size = new System.Drawing.Size(155, 49);
            this.b_second.TabIndex = 7;
            this.b_second.Text = "Другий звіт";
            this.b_second.UseVisualStyleBackColor = true;
            this.b_second.Click += new System.EventHandler(this.b_second_Click);
            // 
            // b_third
            // 
            this.b_third.Location = new System.Drawing.Point(614, 309);
            this.b_third.Name = "b_third";
            this.b_third.Size = new System.Drawing.Size(155, 49);
            this.b_third.TabIndex = 8;
            this.b_third.Text = "Третій звіт";
            this.b_third.UseVisualStyleBackColor = true;
            this.b_third.Click += new System.EventHandler(this.b_third_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 388);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(561, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "1.Список звернень на гарячу лінію за період з можливістю фільтрації по оператору." +
    "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 417);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(639, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "2.Кількість оброблених звернень по оператору в прив’язці до лінії по якій відбуло" +
    "ся звернення.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 446);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(461, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "3.Розбивка всіх звернень по часу коли надійшло звернення за добу.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 517);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.b_third);
            this.Controls.Add(this.b_second);
            this.Controls.Add(this.b_first);
            this.Controls.Add(this.b_main_update_aplication);
            this.Controls.Add(this.b_main_update_hotline);
            this.Controls.Add(this.b_main_update_operator);
            this.Controls.Add(this.b_main_update_abonent);
            this.Controls.Add(this.b_main_output);
            this.Controls.Add(this.b_main_input);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_main_input;
        private System.Windows.Forms.Button b_main_output;
        private System.Windows.Forms.Button b_main_update_abonent;
        private System.Windows.Forms.Button b_main_update_operator;
        private System.Windows.Forms.Button b_main_update_hotline;
        private System.Windows.Forms.Button b_main_update_aplication;
        private System.Windows.Forms.Button b_first;
        private System.Windows.Forms.Button b_second;
        private System.Windows.Forms.Button b_third;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

