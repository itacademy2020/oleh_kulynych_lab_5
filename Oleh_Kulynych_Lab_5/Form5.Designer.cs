﻿namespace Oleh_Kulynych_Lab_5
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_newdateemp_Operator = new System.Windows.Forms.DateTimePicker();
            this.tb_newnumcall_Operator = new System.Windows.Forms.TextBox();
            this.tb_NewPIB_Operator = new System.Windows.Forms.TextBox();
            this.b_back = new System.Windows.Forms.Button();
            this.b_update_operator = new System.Windows.Forms.Button();
            this.dgv_update_operator = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_update_operator)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_newdateemp_Operator);
            this.splitContainer1.Panel1.Controls.Add(this.tb_newnumcall_Operator);
            this.splitContainer1.Panel1.Controls.Add(this.tb_NewPIB_Operator);
            this.splitContainer1.Panel1.Controls.Add(this.b_back);
            this.splitContainer1.Panel1.Controls.Add(this.b_update_operator);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv_update_operator);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(220, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Нова дата прийняття на роботу";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Нова кількість дзвінків";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Новий ПІБ";
            // 
            // dtp_newdateemp_Operator
            // 
            this.dtp_newdateemp_Operator.Location = new System.Drawing.Point(13, 171);
            this.dtp_newdateemp_Operator.Name = "dtp_newdateemp_Operator";
            this.dtp_newdateemp_Operator.Size = new System.Drawing.Size(163, 22);
            this.dtp_newdateemp_Operator.TabIndex = 4;
           
            // 
            // tb_newnumcall_Operator
            // 
            this.tb_newnumcall_Operator.Location = new System.Drawing.Point(13, 107);
            this.tb_newnumcall_Operator.Name = "tb_newnumcall_Operator";
            this.tb_newnumcall_Operator.Size = new System.Drawing.Size(239, 22);
            this.tb_newnumcall_Operator.TabIndex = 3;
            this.tb_newnumcall_Operator.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_newnumcall_Operator_KeyPress);
            // 
            // tb_NewPIB_Operator
            // 
            this.tb_NewPIB_Operator.Location = new System.Drawing.Point(13, 52);
            this.tb_NewPIB_Operator.Name = "tb_NewPIB_Operator";
            this.tb_NewPIB_Operator.Size = new System.Drawing.Size(239, 22);
            this.tb_NewPIB_Operator.TabIndex = 2;
            // 
            // b_back
            // 
            this.b_back.Location = new System.Drawing.Point(13, 415);
            this.b_back.Name = "b_back";
            this.b_back.Size = new System.Drawing.Size(155, 23);
            this.b_back.TabIndex = 1;
            this.b_back.Text = "Повернутись назад";
            this.b_back.UseVisualStyleBackColor = true;
            this.b_back.Click += new System.EventHandler(this.b_back_Click);
            // 
            // b_update_operator
            // 
            this.b_update_operator.Location = new System.Drawing.Point(13, 376);
            this.b_update_operator.Name = "b_update_operator";
            this.b_update_operator.Size = new System.Drawing.Size(152, 23);
            this.b_update_operator.TabIndex = 0;
            this.b_update_operator.Text = "Зберегти зміни";
            this.b_update_operator.UseVisualStyleBackColor = true;
            this.b_update_operator.Click += new System.EventHandler(this.b_update_operator_Click);
            // 
            // dgv_update_operator
            // 
            this.dgv_update_operator.AllowUserToAddRows = false;
            this.dgv_update_operator.AllowUserToDeleteRows = false;
            this.dgv_update_operator.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_update_operator.Location = new System.Drawing.Point(3, 0);
            this.dgv_update_operator.Name = "dgv_update_operator";
            this.dgv_update_operator.ReadOnly = true;
            this.dgv_update_operator.RowHeadersWidth = 51;
            this.dgv_update_operator.RowTemplate.Height = 24;
            this.dgv_update_operator.Size = new System.Drawing.Size(527, 450);
            this.dgv_update_operator.TabIndex = 0;
            this.dgv_update_operator.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_update_operator_CellClick);
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form5";
            this.Text = "Form5";
            this.Load += new System.EventHandler(this.Form5_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_update_operator)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox tb_newnumcall_Operator;
        private System.Windows.Forms.TextBox tb_NewPIB_Operator;
        private System.Windows.Forms.Button b_back;
        private System.Windows.Forms.Button b_update_operator;
        private System.Windows.Forms.DataGridView dgv_update_operator;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp_newdateemp_Operator;
    }
}