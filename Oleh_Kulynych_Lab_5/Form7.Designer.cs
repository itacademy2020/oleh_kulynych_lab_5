﻿namespace Oleh_Kulynych_Lab_5
{
    partial class Form7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cb_new_hotline = new System.Windows.Forms.ComboBox();
            this.cb_new_abonent = new System.Windows.Forms.ComboBox();
            this.cb_new_operator = new System.Windows.Forms.ComboBox();
            this.tb_newtopic_aplication = new System.Windows.Forms.TextBox();
            this.dtp_newfinishaplication = new System.Windows.Forms.DateTimePicker();
            this.dtp_newtimestartaplication = new System.Windows.Forms.DateTimePicker();
            this.dtp_newtimefinishaplication = new System.Windows.Forms.DateTimePicker();
            this.b_update_aplication = new System.Windows.Forms.Button();
            this.b_back = new System.Windows.Forms.Button();
            this.dgv_update_aplication = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dtp_newstartaplication = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_update_aplication)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dtp_newstartaplication);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.b_back);
            this.splitContainer1.Panel1.Controls.Add(this.b_update_aplication);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_newtimefinishaplication);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_newtimestartaplication);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_newfinishaplication);
            this.splitContainer1.Panel1.Controls.Add(this.tb_newtopic_aplication);
            this.splitContainer1.Panel1.Controls.Add(this.cb_new_operator);
            this.splitContainer1.Panel1.Controls.Add(this.cb_new_abonent);
            this.splitContainer1.Panel1.Controls.Add(this.cb_new_hotline);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv_update_aplication);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 228;
            this.splitContainer1.TabIndex = 0;
            // 
            // cb_new_hotline
            // 
            this.cb_new_hotline.FormattingEnabled = true;
            this.cb_new_hotline.Location = new System.Drawing.Point(13, 32);
            this.cb_new_hotline.Name = "cb_new_hotline";
            this.cb_new_hotline.Size = new System.Drawing.Size(200, 24);
            this.cb_new_hotline.TabIndex = 0;
            // 
            // cb_new_abonent
            // 
            this.cb_new_abonent.FormattingEnabled = true;
            this.cb_new_abonent.Location = new System.Drawing.Point(13, 79);
            this.cb_new_abonent.Name = "cb_new_abonent";
            this.cb_new_abonent.Size = new System.Drawing.Size(200, 24);
            this.cb_new_abonent.TabIndex = 1;
            // 
            // cb_new_operator
            // 
            this.cb_new_operator.FormattingEnabled = true;
            this.cb_new_operator.Location = new System.Drawing.Point(13, 126);
            this.cb_new_operator.Name = "cb_new_operator";
            this.cb_new_operator.Size = new System.Drawing.Size(200, 24);
            this.cb_new_operator.TabIndex = 2;
            // 
            // tb_newtopic_aplication
            // 
            this.tb_newtopic_aplication.Location = new System.Drawing.Point(13, 177);
            this.tb_newtopic_aplication.Name = "tb_newtopic_aplication";
            this.tb_newtopic_aplication.Size = new System.Drawing.Size(200, 22);
            this.tb_newtopic_aplication.TabIndex = 3;
            // 
            // dtp_newfinishaplication
            // 
            this.dtp_newfinishaplication.Location = new System.Drawing.Point(13, 268);
            this.dtp_newfinishaplication.Name = "dtp_newfinishaplication";
            this.dtp_newfinishaplication.Size = new System.Drawing.Size(200, 22);
            this.dtp_newfinishaplication.TabIndex = 4;
            // 
            // dtp_newtimestartaplication
            // 
            this.dtp_newtimestartaplication.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtp_newtimestartaplication.Location = new System.Drawing.Point(13, 313);
            this.dtp_newtimestartaplication.Name = "dtp_newtimestartaplication";
            this.dtp_newtimestartaplication.ShowUpDown = true;
            this.dtp_newtimestartaplication.Size = new System.Drawing.Size(200, 22);
            this.dtp_newtimestartaplication.TabIndex = 6;
            // 
            // dtp_newtimefinishaplication
            // 
            this.dtp_newtimefinishaplication.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtp_newtimefinishaplication.Location = new System.Drawing.Point(13, 358);
            this.dtp_newtimefinishaplication.Name = "dtp_newtimefinishaplication";
            this.dtp_newtimefinishaplication.ShowUpDown = true;
            this.dtp_newtimefinishaplication.Size = new System.Drawing.Size(200, 22);
            this.dtp_newtimefinishaplication.TabIndex = 7;
            // 
            // b_update_aplication
            // 
            this.b_update_aplication.Location = new System.Drawing.Point(16, 386);
            this.b_update_aplication.Name = "b_update_aplication";
            this.b_update_aplication.Size = new System.Drawing.Size(162, 23);
            this.b_update_aplication.TabIndex = 8;
            this.b_update_aplication.Text = "Зберегти зміни";
            this.b_update_aplication.UseVisualStyleBackColor = true;
            this.b_update_aplication.Click += new System.EventHandler(this.b_update_aplication_Click);
            // 
            // b_back
            // 
            this.b_back.Location = new System.Drawing.Point(16, 415);
            this.b_back.Name = "b_back";
            this.b_back.Size = new System.Drawing.Size(162, 23);
            this.b_back.TabIndex = 9;
            this.b_back.Text = "Повернутись назад";
            this.b_back.UseVisualStyleBackColor = true;
            this.b_back.Click += new System.EventHandler(this.b_back_Click);
            // 
            // dgv_update_aplication
            // 
            this.dgv_update_aplication.AllowUserToAddRows = false;
            this.dgv_update_aplication.AllowUserToDeleteRows = false;
            this.dgv_update_aplication.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_update_aplication.Location = new System.Drawing.Point(3, 0);
            this.dgv_update_aplication.Name = "dgv_update_aplication";
            this.dgv_update_aplication.ReadOnly = true;
            this.dgv_update_aplication.RowHeadersWidth = 51;
            this.dgv_update_aplication.RowTemplate.Height = 24;
            this.dgv_update_aplication.Size = new System.Drawing.Size(565, 450);
            this.dgv_update_aplication.TabIndex = 0;
            this.dgv_update_aplication.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_update_aplication_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Нова лінія";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Новий абонент";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Новий оператор";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Нова тема";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "Дата початку";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 248);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Дата кінця";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 293);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Час початку";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 338);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Час кінця";
            // 
            // dtp_newstartaplication
            // 
            this.dtp_newstartaplication.Location = new System.Drawing.Point(13, 224);
            this.dtp_newstartaplication.Name = "dtp_newstartaplication";
            this.dtp_newstartaplication.Size = new System.Drawing.Size(200, 22);
            this.dtp_newstartaplication.TabIndex = 4;
            // 
            // Form7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form7";
            this.Text = "Form7";
            this.Load += new System.EventHandler(this.Form7_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_update_aplication)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button b_back;
        private System.Windows.Forms.Button b_update_aplication;
        private System.Windows.Forms.DateTimePicker dtp_newtimefinishaplication;
        private System.Windows.Forms.DateTimePicker dtp_newtimestartaplication;
        private System.Windows.Forms.DateTimePicker dtp_newfinishaplication;
        private System.Windows.Forms.TextBox tb_newtopic_aplication;
        private System.Windows.Forms.ComboBox cb_new_operator;
        private System.Windows.Forms.ComboBox cb_new_abonent;
        private System.Windows.Forms.ComboBox cb_new_hotline;
        private System.Windows.Forms.DataGridView dgv_update_aplication;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtp_newstartaplication;
    }
}