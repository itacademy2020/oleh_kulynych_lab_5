﻿namespace Oleh_Kulynych_Lab_5
{
    partial class Form8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.b_active = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_seconddate = new System.Windows.Forms.DateTimePicker();
            this.dtp_firstdate = new System.Windows.Forms.DateTimePicker();
            this.cb_operator = new System.Windows.Forms.ComboBox();
            this.b_back = new System.Windows.Forms.Button();
            this.dgv_first = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_first)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.b_active);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_seconddate);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_firstdate);
            this.splitContainer1.Panel1.Controls.Add(this.cb_operator);
            this.splitContainer1.Panel1.Controls.Add(this.b_back);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv_first);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 180;
            this.splitContainer1.TabIndex = 0;
            // 
            // b_active
            // 
            this.b_active.Location = new System.Drawing.Point(13, 216);
            this.b_active.Name = "b_active";
            this.b_active.Size = new System.Drawing.Size(114, 23);
            this.b_active.TabIndex = 7;
            this.b_active.Text = "Скласти звіт";
            this.b_active.UseVisualStyleBackColor = true;
            this.b_active.Click += new System.EventHandler(this.b_active_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "По:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "З:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Оператор";
            // 
            // dtp_seconddate
            // 
            this.dtp_seconddate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_seconddate.Location = new System.Drawing.Point(13, 161);
            this.dtp_seconddate.Name = "dtp_seconddate";
            this.dtp_seconddate.Size = new System.Drawing.Size(145, 22);
            this.dtp_seconddate.TabIndex = 3;
            // 
            // dtp_firstdate
            // 
            this.dtp_firstdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_firstdate.Location = new System.Drawing.Point(13, 102);
            this.dtp_firstdate.Name = "dtp_firstdate";
            this.dtp_firstdate.Size = new System.Drawing.Size(145, 22);
            this.dtp_firstdate.TabIndex = 2;
            this.dtp_firstdate.ValueChanged += new System.EventHandler(this.dtp_firstdate_ValueChanged);
            // 
            // cb_operator
            // 
            this.cb_operator.FormattingEnabled = true;
            this.cb_operator.Location = new System.Drawing.Point(12, 33);
            this.cb_operator.Name = "cb_operator";
            this.cb_operator.Size = new System.Drawing.Size(121, 24);
            this.cb_operator.TabIndex = 1;
            // 
            // b_back
            // 
            this.b_back.Location = new System.Drawing.Point(12, 415);
            this.b_back.Name = "b_back";
            this.b_back.Size = new System.Drawing.Size(146, 23);
            this.b_back.TabIndex = 0;
            this.b_back.Text = "Повернутися назад";
            this.b_back.UseVisualStyleBackColor = true;
            this.b_back.Click += new System.EventHandler(this.b_back_Click);
            // 
            // dgv_first
            // 
            this.dgv_first.AllowUserToAddRows = false;
            this.dgv_first.AllowUserToDeleteRows = false;
            this.dgv_first.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_first.Location = new System.Drawing.Point(3, 0);
            this.dgv_first.Name = "dgv_first";
            this.dgv_first.ReadOnly = true;
            this.dgv_first.RowHeadersWidth = 51;
            this.dgv_first.RowTemplate.Height = 24;
            this.dgv_first.Size = new System.Drawing.Size(613, 450);
            this.dgv_first.TabIndex = 0;
            // 
            // Form8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form8";
            this.Text = "Form8";
            this.Load += new System.EventHandler(this.Form8_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_first)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button b_back;
        private System.Windows.Forms.DataGridView dgv_first;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp_seconddate;
        private System.Windows.Forms.DateTimePicker dtp_firstdate;
        private System.Windows.Forms.ComboBox cb_operator;
        private System.Windows.Forms.Button b_active;
    }
}