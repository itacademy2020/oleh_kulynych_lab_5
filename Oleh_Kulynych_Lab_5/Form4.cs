﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Oleh_Kulynych_Lab_5
{
    public partial class Form4 : Form
    {
        
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Kulynych;User ID = Kulynych;Password=Fvcx03756;Trusted_Connection=False;Encrypt=True;";
        private SqlCommandBuilder sqlcombuild = null;
        private SqlConnection sqlConnection = null;
        private SqlDataAdapter sqlDataAdapter = null;
        private DataSet dataSet = null;
        int indexRow;
        public Form4()
        {
            InitializeComponent();
        }

        private void b_back_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            sqlConnection = new SqlConnection(sqlconnectionstring);
            sqlConnection.Open();
            LoadData();
            sqlConnection.Close();
        }
        private void LoadData()
        {
            try
            {
                sqlDataAdapter = new SqlDataAdapter("SELECT * FROM [dbo].[Abonent];", sqlConnection);
                sqlcombuild = new SqlCommandBuilder(sqlDataAdapter);
                sqlcombuild.GetUpdateCommand();

                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "Abonent");
                dgv_Result_Update.DataSource = dataSet.Tables["Abonent"];
                
            }
            catch(Exception ex)
            {
                string message = "Помилка при виведенні записів з таблиці Abonent.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }

        private void dgv_Result_Update_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                indexRow = e.RowIndex;
                DataGridViewRow row = dgv_Result_Update.Rows[indexRow];
                tb_NewPIB.Text = row.Cells[1].Value.ToString();
                tb_NewContact.Text = row.Cells[2].Value.ToString();
            }
            catch(Exception exp)
            {
                string message = "Помилка. Виберіть конкретного абонента. ";
                MessageBox.Show(message,"Помилка", MessageBoxButtons.OK);
            }            
        }

        private void b_Update_Click(object sender, EventArgs e)
        {
            if(tb_NewPIB.Text.ToString() != "")
            {
                if(tb_NewContact.Text.ToString() != "")
                {
                    try
                    {
                        sqlConnection.Open();
                        DataGridViewRow newDataRow = dgv_Result_Update.Rows[indexRow];
                        newDataRow.Cells[1].Value = tb_NewPIB.Text;
                        newDataRow.Cells[2].Value = tb_NewContact.Text;
                        SqlCommand command = new SqlCommand("UPDATE [dbo].[Abonent] SET [PIB] = '" + tb_NewPIB.Text + "', [Contacts] = '" + tb_NewContact.Text + "' WHERE [ID] = " + newDataRow.Cells[0].Value + ";", sqlConnection);
                        command.ExecuteNonQuery();
                        sqlConnection.Close();

                        string message = "Абонент змінений успішно.";
                        MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
                        dgv_Result_Update.Refresh();


                    }
                    catch (Exception ex)
                    {
                        string message = "Помилка при редагуванні оператора.";
                        MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    string message = "Виберіть оператора.";
                    MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                }

            }
            else
            {
                string message = "Виберіть оператора.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
           
        }

        private void tb_NewContact_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }
    }
}
