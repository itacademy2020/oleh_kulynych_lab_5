﻿namespace Oleh_Kulynych_Lab_5
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_back = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_NewContact = new System.Windows.Forms.TextBox();
            this.tb_NewPIB = new System.Windows.Forms.TextBox();
            this.b_Update = new System.Windows.Forms.Button();
            this.dgv_Result_Update = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Result_Update)).BeginInit();
            this.SuspendLayout();
            // 
            // b_back
            // 
            this.b_back.Location = new System.Drawing.Point(12, 427);
            this.b_back.Name = "b_back";
            this.b_back.Size = new System.Drawing.Size(166, 23);
            this.b_back.TabIndex = 1;
            this.b_back.Text = "Повернутись назад";
            this.b_back.UseVisualStyleBackColor = true;
            this.b_back.Click += new System.EventHandler(this.b_back_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, -1);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.tb_NewContact);
            this.splitContainer1.Panel1.Controls.Add(this.tb_NewPIB);
            this.splitContainer1.Panel1.Controls.Add(this.b_Update);
            this.splitContainer1.Panel1.Controls.Add(this.b_back);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv_Result_Update);
            this.splitContainer1.Size = new System.Drawing.Size(803, 453);
            this.splitContainer1.SplitterDistance = 186;
            this.splitContainer1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Нові контакти";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Новий ПІБ";
            // 
            // tb_NewContact
            // 
            this.tb_NewContact.Location = new System.Drawing.Point(13, 108);
            this.tb_NewContact.Name = "tb_NewContact";
            this.tb_NewContact.Size = new System.Drawing.Size(165, 22);
            this.tb_NewContact.TabIndex = 5;
            this.tb_NewContact.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_NewContact_KeyPress);
            // 
            // tb_NewPIB
            // 
            this.tb_NewPIB.Location = new System.Drawing.Point(13, 59);
            this.tb_NewPIB.Name = "tb_NewPIB";
            this.tb_NewPIB.Size = new System.Drawing.Size(165, 22);
            this.tb_NewPIB.TabIndex = 4;
            // 
            // b_Update
            // 
            this.b_Update.Location = new System.Drawing.Point(13, 374);
            this.b_Update.Name = "b_Update";
            this.b_Update.Size = new System.Drawing.Size(141, 25);
            this.b_Update.TabIndex = 3;
            this.b_Update.Text = "Зберегти зміни";
            this.b_Update.UseVisualStyleBackColor = true;
            this.b_Update.Click += new System.EventHandler(this.b_Update_Click);
            // 
            // dgv_Result_Update
            // 
            this.dgv_Result_Update.AllowUserToAddRows = false;
            this.dgv_Result_Update.AllowUserToDeleteRows = false;
            this.dgv_Result_Update.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Result_Update.Location = new System.Drawing.Point(3, 3);
            this.dgv_Result_Update.Name = "dgv_Result_Update";
            this.dgv_Result_Update.ReadOnly = true;
            this.dgv_Result_Update.RowHeadersWidth = 51;
            this.dgv_Result_Update.RowTemplate.Height = 24;
            this.dgv_Result_Update.Size = new System.Drawing.Size(607, 447);
            this.dgv_Result_Update.TabIndex = 0;
            this.dgv_Result_Update.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Result_Update_CellClick);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form4";
            this.Text = "Form4";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Result_Update)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button b_back;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button b_Update;
        private System.Windows.Forms.DataGridView dgv_Result_Update;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_NewContact;
        private System.Windows.Forms.TextBox tb_NewPIB;
    }
}