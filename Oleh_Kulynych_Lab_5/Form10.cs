﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Oleh_Kulynych_Lab_5
{
    public partial class Form10 : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Kulynych;User ID = Kulynych;Password=Fvcx03756;Trusted_Connection=False;Encrypt=True;";
        public Form10()
        {
            InitializeComponent();
        }

        private void b_back_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }

        private void b_active_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            try
            {
                con.Open();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("SELECT [timestart_aplication], COUNT([Aplication].[ID]) AS [count] FROM [dbo].[Aplication] WHERE [Aplication].[datestart_aplication] = '"+dtp_third.Value.Date.ToString("yyyy-MM-dd")+"' GROUP BY [timestart_aplication] ", con);
                DataSet dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "Aplication");
                dgv_third.DataSource = dataSet.Tables["Aplication"];
                dgv_third.Refresh();
                string message = "Звіт складений успішно.";
                MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
            }
            catch (Exception exp)
            {
                string message = "Виникла помилка при складанні звіту.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
            finally
            {
                con.Close();
            }
        }
    }
}
