﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lab8_Oleh_Kulynych_
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class KulynychEntities : DbContext
    {
        public KulynychEntities()
            : base("name=KulynychEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Operator> Operator { get; set; }
    
        public virtual int p_Add_Operator(string operator_PIB, Nullable<System.DateTime> date_employment, Nullable<int> number_calls)
        {
            var operator_PIBParameter = operator_PIB != null ?
                new ObjectParameter("Operator_PIB", operator_PIB) :
                new ObjectParameter("Operator_PIB", typeof(string));
    
            var date_employmentParameter = date_employment.HasValue ?
                new ObjectParameter("date_employment", date_employment) :
                new ObjectParameter("date_employment", typeof(System.DateTime));
    
            var number_callsParameter = number_calls.HasValue ?
                new ObjectParameter("number_calls", number_calls) :
                new ObjectParameter("number_calls", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("p_Add_Operator", operator_PIBParameter, date_employmentParameter, number_callsParameter);
        }
    
        public virtual int p_Update_Operator(Nullable<int> operator_ID, string operator_PIB, Nullable<System.DateTime> date_employment, Nullable<int> number_calls)
        {
            var operator_IDParameter = operator_ID.HasValue ?
                new ObjectParameter("Operator_ID", operator_ID) :
                new ObjectParameter("Operator_ID", typeof(int));
    
            var operator_PIBParameter = operator_PIB != null ?
                new ObjectParameter("Operator_PIB", operator_PIB) :
                new ObjectParameter("Operator_PIB", typeof(string));
    
            var date_employmentParameter = date_employment.HasValue ?
                new ObjectParameter("date_employment", date_employment) :
                new ObjectParameter("date_employment", typeof(System.DateTime));
    
            var number_callsParameter = number_calls.HasValue ?
                new ObjectParameter("number_calls", number_calls) :
                new ObjectParameter("number_calls", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("p_Update_Operator", operator_IDParameter, operator_PIBParameter, date_employmentParameter, number_callsParameter);
        }
    }
}
