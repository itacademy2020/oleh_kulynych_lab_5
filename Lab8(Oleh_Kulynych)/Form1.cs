﻿using System;
using System.Linq;
using System.Windows.Forms;
namespace Lab8_Oleh_Kulynych_
{
    public partial class Form1 : Form
    {
        KulynychEntities DB = new KulynychEntities();
        int indexRow;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dgv_result_table.DataSource = DB.Operator.ToList();
        }

        private void tb_num_calls_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void b_Add_New_Operator_Click(object sender, EventArgs e)
        {
            try
            {
                if (tb_Operator_PIB.Text.ToString() != "")
                {
                    if(tb_num_calls.Text.ToString() != "")
                    {
                        DB.p_Add_Operator(tb_Operator_PIB.Text.ToString(), Convert.ToDateTime(dtp_date_employment.Value.Date.ToString("yyyy-MM-dd")), Convert.ToInt32(tb_num_calls.Text.ToString()));
                        string message = "Оператор доданий.";
                        MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
                        dgv_result_table.DataSource = DB.Operator.ToList();
                    }
                    else
                    {
                        string message = "Введіть кількість дзвінків.";
                        MessageBox.Show(message, "Error", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    string message = "Введіть прізвище.";
                    MessageBox.Show(message, "Error", MessageBoxButtons.OK);
                }
            }
            catch(Exception ex)
            {
                string message = "An error occurred while adding the operator." + ex.Message;
                MessageBox.Show(message, "Error", MessageBoxButtons.OK);
            }          
        }

        private void dgv_result_table_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                indexRow = e.RowIndex;
                DataGridViewRow row = dgv_result_table.Rows[indexRow];
                tb_Operator_PIB.Text = row.Cells[1].Value.ToString();
                dtp_date_employment.Text = row.Cells[2].Value.ToString();
                tb_num_calls.Text = row.Cells[3].Value.ToString();
            }
            catch (Exception exp)
            {
                string message = "Помилка. Виберіть конкретного оператора. ";
                MessageBox.Show(message, "Error", MessageBoxButtons.OK);
            }
        }

        private void b_Update_Operator_Click(object sender, EventArgs e)
        {
            if (tb_Operator_PIB.Text.ToString() != "")
            {
                if (tb_num_calls.Text.ToString() != "")
                {
                    try
                    {
                        DataGridViewRow newDataRow = dgv_result_table.Rows[indexRow];
                        newDataRow.Cells[1].Value = tb_Operator_PIB.Text;
                        newDataRow.Cells[2].Value = dtp_date_employment.Text;
                        newDataRow.Cells[3].Value = tb_num_calls.Text;
                        DB.p_Update_Operator(Convert.ToInt32(newDataRow.Cells[0].Value),tb_Operator_PIB.Text.ToString(), Convert.ToDateTime(dtp_date_employment.Value.Date.ToString("yyyy-MM-dd")), Convert.ToInt32(tb_num_calls.Text.ToString()));

                        string message = "Оператор змінений успішно.";
                        MessageBox.Show(message, "Успіх", MessageBoxButtons.OK);
                        dgv_result_table.Refresh();


                    }
                    catch (Exception ex)
                    {
                        string message = "Помилка при редагуванні оператора.";
                        MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    string message = "Виберіть оператора.";
                    MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
                }
            }
            else
            {
                string message = "Виберіть оператора.";
                MessageBox.Show(message, "Помилка", MessageBoxButtons.OK);
            }
        }
    }
}
