﻿namespace Lab8_Oleh_Kulynych_
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_date_employment = new System.Windows.Forms.DateTimePicker();
            this.tb_num_calls = new System.Windows.Forms.TextBox();
            this.tb_Operator_PIB = new System.Windows.Forms.TextBox();
            this.b_Add_New_Operator = new System.Windows.Forms.Button();
            this.dgv_result_table = new System.Windows.Forms.DataGridView();
            this.b_Update_Operator = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_result_table)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.b_Update_Operator);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_date_employment);
            this.splitContainer1.Panel1.Controls.Add(this.tb_num_calls);
            this.splitContainer1.Panel1.Controls.Add(this.tb_Operator_PIB);
            this.splitContainer1.Panel1.Controls.Add(this.b_Add_New_Operator);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgv_result_table);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 216;
            this.splitContainer1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "К-сть дзвінків";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(186, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Дата прийняття на роботу";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "ПІБ";
            // 
            // dtp_date_employment
            // 
            this.dtp_date_employment.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_date_employment.Location = new System.Drawing.Point(13, 94);
            this.dtp_date_employment.Name = "dtp_date_employment";
            this.dtp_date_employment.Size = new System.Drawing.Size(172, 22);
            this.dtp_date_employment.TabIndex = 3;
            // 
            // tb_num_calls
            // 
            this.tb_num_calls.Location = new System.Drawing.Point(13, 151);
            this.tb_num_calls.Name = "tb_num_calls";
            this.tb_num_calls.Size = new System.Drawing.Size(172, 22);
            this.tb_num_calls.TabIndex = 2;
            this.tb_num_calls.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_num_calls_KeyPress);
            // 
            // tb_Operator_PIB
            // 
            this.tb_Operator_PIB.Location = new System.Drawing.Point(13, 38);
            this.tb_Operator_PIB.Name = "tb_Operator_PIB";
            this.tb_Operator_PIB.Size = new System.Drawing.Size(172, 22);
            this.tb_Operator_PIB.TabIndex = 1;
            // 
            // b_Add_New_Operator
            // 
            this.b_Add_New_Operator.Location = new System.Drawing.Point(12, 352);
            this.b_Add_New_Operator.Name = "b_Add_New_Operator";
            this.b_Add_New_Operator.Size = new System.Drawing.Size(159, 32);
            this.b_Add_New_Operator.TabIndex = 0;
            this.b_Add_New_Operator.Text = "Додати оператора";
            this.b_Add_New_Operator.UseVisualStyleBackColor = true;
            this.b_Add_New_Operator.Click += new System.EventHandler(this.b_Add_New_Operator_Click);
            // 
            // dgv_result_table
            // 
            this.dgv_result_table.AllowUserToAddRows = false;
            this.dgv_result_table.AllowUserToDeleteRows = false;
            this.dgv_result_table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_result_table.Location = new System.Drawing.Point(3, 0);
            this.dgv_result_table.Name = "dgv_result_table";
            this.dgv_result_table.ReadOnly = true;
            this.dgv_result_table.RowHeadersWidth = 51;
            this.dgv_result_table.RowTemplate.Height = 24;
            this.dgv_result_table.Size = new System.Drawing.Size(577, 450);
            this.dgv_result_table.TabIndex = 0;
            this.dgv_result_table.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_result_table_CellClick);
            // 
            // b_Update_Operator
            // 
            this.b_Update_Operator.Location = new System.Drawing.Point(12, 390);
            this.b_Update_Operator.Name = "b_Update_Operator";
            this.b_Update_Operator.Size = new System.Drawing.Size(159, 45);
            this.b_Update_Operator.TabIndex = 7;
            this.b_Update_Operator.Text = "Редагувати оператора";
            this.b_Update_Operator.UseVisualStyleBackColor = true;
            this.b_Update_Operator.Click += new System.EventHandler(this.b_Update_Operator_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_result_table)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button b_Add_New_Operator;
        private System.Windows.Forms.DataGridView dgv_result_table;
        private System.Windows.Forms.DateTimePicker dtp_date_employment;
        private System.Windows.Forms.TextBox tb_num_calls;
        private System.Windows.Forms.TextBox tb_Operator_PIB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button b_Update_Operator;
    }
}

